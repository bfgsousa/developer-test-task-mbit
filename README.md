# Developer Test Task MBit

- Script using PHP 7.4
- Requires at least PHP 7.x

## Usage  

`php calculator.php [n1] [operator] [n2]`  

### Operators

- Substract (-)
- Divide (/)
- Multiply (x)
- Sum (+)
