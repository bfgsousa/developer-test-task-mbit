<?php

class OperationValidation
{
    private array $operations;
    private string $operator;

    public function __construct(string $operator, array $operations)
    {
        $this->operator = $operator;
        $this->operations = $operations;
    }

    public function getClass($op)
    {
        foreach($this->operations as $operation) {
            if($operation::getOperator() == $this->operator) {
                return $operation;
            }
        }

        return false;
    }
}