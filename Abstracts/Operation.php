<?php

abstract class Operation
{
    protected float $n1;
    protected float $n2;

    public function __construct(string $n1, string $n2)
    {
        $this->n1 = $this->format("First", $n1);
        $this->n2 = $this->format("Second", $n2);
    }

    abstract public static function getOperator() : string;
    abstract public function calculate() : float;

    private function format(string $label, string $number) : float
    {
        $number = str_replace(",", ".", $number);

        if(!is_numeric($number)) {
            throw new ValidNumberException("$label Number must have a valid type");
        }

        return (float) $number;
    }
}