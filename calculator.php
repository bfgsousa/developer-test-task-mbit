<?php

if(php_sapi_name() !== 'cli') {
    echo PHP_EOL . "This script must run on CLI. Ex: php calculator.php [n1] [operator] [n2]" . PHP_EOL;
    die;
}

require_once './Abstracts/Operation.php';
require_once './Operations/Divide.php';
require_once './Operations/Multiply.php';
require_once './Operations/Subtract.php';
require_once './Operations/Sum.php';

require_once './Exceptions/BaseException.php';
require_once './Exceptions/InfiniteNumberException.php';
require_once './Exceptions/UndefinedNumberException.php';
require_once './Exceptions/ValidNumberException.php';

require_once './Validations/OperationValidation.php';

$operations = [
    Subtract::class,
    Divide::class,
    Multiply::class,
    Sum::class
];

unset($argv[0]);
$args = $argv;

if(sizeof($args) != 3) {
    echo PHP_EOL . "Required three parameters. Ex: php calculator.php 54.44 x 22.22" . PHP_EOL;
    echo PHP_EOL . "Operators:" . PHP_EOL;
    echo PHP_EOL;
    echo "Divide: /"  . PHP_EOL;
    echo "Multiply: x"  . PHP_EOL;
    echo "Subtract: -"  . PHP_EOL;
    echo "Sum: +"  . PHP_EOL;
    die;
}

$n1 = $args[1];
$n2 = $args[3];

$operator = $args[2];

$operationValidation = new OperationValidation(
    $operator,
    $operations
);

$operationClass = $operationValidation->getClass($operator);

if(!$operationClass) {
    echo "Operation invalid type: $operator" . PHP_EOL;
    echo PHP_EOL . "Operators:" . PHP_EOL;
    echo PHP_EOL;
    echo "Divide: /"  . PHP_EOL;
    echo "Multiply: x"  . PHP_EOL;
    echo "Subtract: -"  . PHP_EOL;
    echo "Sum: +"  . PHP_EOL;
    die;
}

try {
    $result = (new $operationClass($n1, $n2))->calculate($n1, $n2);
    echo PHP_EOL . "$n1 $operator $n2 = $result";
} catch (InfiniteNumberException $e) {
    echo $e->getMessage() . PHP_EOL;
} catch (UndefinedNumberException $e) {
    echo $e->getMessage() . PHP_EOL;
} catch (ValidNumberException $e) {
    echo $e->getMessage() . PHP_EOL;
} catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}