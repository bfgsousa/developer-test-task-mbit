<?php

class Divide extends Operation
{
    const OPERATOR = "/";

    public static function getOperator() : string
    {
        return self::OPERATOR;
    }

    public function calculate() : float
    {
        if($this->n1 == 0 && $this->n2 == 0) {
            
            throw new UndefinedNumberException("Both numbers zero result on NAN/undefined Result");
        }

        if($this->n2 == 0) {
            throw new InfiniteNumberException("Second number result on Infinite Number Result");
        }

        return $this->n1/$this->n2;
    }
}