<?php

class Sum extends Operation
{
    const OPERATOR = "+";

    public static function getOperator() : string
    {
        return self::OPERATOR;
    }

    public function calculate() : float
    {
        return $this->n1+$this->n2;
    }
}