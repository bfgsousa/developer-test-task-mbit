<?php

class Multiply extends Operation
{
    const OPERATOR = "x";

    public static function getOperator() : string
    {
        return self::OPERATOR;
    }

    public function calculate() : float
    {
        return $this->n1*$this->n2;
    }
}